// !Задание 1
// var actor = {
//     name: "Michael",
//     firstName: "Ivanov",
//     country: "Russia",
//     city: "Machachkala",
//     hasOskar: false,
//     filmsCount: 10,
//     age: "14",
//     languages: ["RU-ru", "EN-us", "TR-tr"],
// };
// // чтоб приходил объект в аргумент функции добавил типизацию. Так как actor является аргументом, то нужно прописать для него или интерфейс, или привести actor.age к строке? тоб сработала конкатенация.
// var howOldWillBeActorAfterTwentyYears = function (actor) {
//     return (Number(actor.age) + 20).toString();
// };
// console.log(howOldWillBeActorAfterTwentyYears(actor)); // '1420'
// // !Задание 2
// document.addEventListener("click", (e) => {
//   const coords = [e.pageX, e.pageY];
//   // событие posX отсутсвует в объектк event, вероятно имелось ввиду pageX
//   console.log(`Point is ${coords[0]}, ${coords[1]}`);
// });
// !Задание 3. Использование нетипизированного кода
// функция принимает массив(об этом не указано нигде), проходит по каждому элементу массива(который является объектом, так как есть обращение к ключам)
// в случае, если выполняется условие, что возраст больше 18 и указан пол, то прибавляем 1(так как булево значение приводится к 0 и 1)
// очевидно,что первая функция для подсчета людей, которые старше 18 и у них обозначен пол(в ней ошибка в reduce). Вторая функция(тоже ошибка в reduce) для подсчета мужчин старше 18 лет, при этом через type нам говорят, что каждый элемент массива является объектом, ключи которого имеют след типы.
// function someFunc(data) {
//   return data.reduce(
//     (acc, current) => acc + Number(current.age > 18 && current.isMale),
//     0
//   );
// }
// type Human = {
//   name: string;
//   age: number;
//   gender: "male" | "female";
// };
// function someFunc1(data: Human[]): number {
//   return data.reduce((acc: number, current: Human): number => {
//     return acc + Number(current.age > 18 && current.gender === "male");
//   }, 0);
// }
// let test: Array = [
//   {
//     age: 10,
//     isMale: true,
//   },
//   {
//     age: 20,
//     isMale: false,
//   },
//   {
//     age: 23,
//     isMale: true,
//   },
//   {
//     age: 3,
//     isMale: true,
//   },
// ];
// !Задание 4. Написание кода
// function reverseWord(str: string): string {
//   let newStr = str.split(" ");
//   let newString = newStr.map((el) => {
//     return (el = el.split("").reverse().join(''));
//   });
// let res:string=newString.join(' ')
// return res
// }
// reverseWord("Hello, world my friends");
// !Задание 5. Алгоритмическая задача
// function getSquareElement(number: number): number {
//   if (number < 0) return number;
//   else {
//     let digits: String = number
//       .toString()
//       .split("")
//       .map((el) => {
//         return (Number(el) ** 2).toString();
//       })
//       .join("");
//     let res: number = Number(digits);
//     console.log(res);
//     return res;
//   }
// }
// getSquareElement(124);
// !Задание 6. Алгоритмическая задача — 2
// function digitalRoot(number: number): number {
//   if(number>0) {
//     let digits: string[] = number.toString().split("");
//     recurs(digits)
//     function recurs(arr:string[]) {
//       digits=digits.reduce((acc, current)=>acc+Number(current),0).toString().split('')
//       if(arr.length!=1) {
//         return recurs(digits)
//       } else{
//         return digits
//       } 
//     }
//     console.log(+digits);
//     return +digits
//   } else if (number === 0) return 0
//   else return number
// }
// digitalRoot(942);
// digitalRoot(493193);
// console.log(digitalRoot(-493193));
// console.log(digitalRoot(0));
