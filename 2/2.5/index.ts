// !Задание 5. Объекты v2 — код

let obj1 = {
  a: 1,
  b: "string",
  c: true,
  d: {
    1: 12,
    2: 22,
    3: 33,
    4: {
      aa: false,
      bb: false,
    },
  },
};

let obj2 = {
  a: 1,
  b: "string",
  c: true,
  d: {
    1: 12,
    2: 22,
    3: 33,
    4: {
      aa: false,
      bb: false,
    },
  },
};

let obj4 = {
  a: 1,
  b: "string",
  c: true,
  d: {
    1: 12,
    2: 22,
    3: 33,
    4: true,
  },
};

let obj3 = {
  a: 1,
  b: "string",
  c: false,
};

type SimpleObject = {
  [key: string]: unknown;
};

function areEqual(obj1: SimpleObject, obj2: SimpleObject): boolean {
  let objKeys1 = Object.keys(obj1);
  let objKeys2 = Object.keys(obj2);

  if (objKeys1.length !== objKeys2.length) {
    return false;
  }

  for (let el of objKeys1) {
    if (!objKeys2.includes(el)) {
      return false;
    } else if (obj1[el] !== obj2[el]) {
      return false;
    }
  }
  return true;
}
console.log(areEqual(obj1, obj2));
console.log(areEqual(obj1, obj3));

function newEqual(obj1: object, obj2: object): boolean {
  let objKeys1 = Object.keys(obj1);
  let objKeys2 = Object.keys(obj2);

  if (objKeys1.length !== objKeys2.length) {
    return false;
  }
  let type3: SimpleObject = { w: 1 };
  console.log(typeof type3);

  for (let el of objKeys1) {
    let typeEl1 = typeof obj1[el];
    let typeEl2 = typeof obj2[el];
    console.log(typeof type3);

    if (!objKeys2.includes(el)) {
      return false;
    } else if (
      obj1[el] !== obj2[el] &&
      typeEl1 !== "object" &&
      typeEl2 !== "object"
    ) {
      return false;
    } else if (typeEl1 == "object" && typeEl2 == "object") {
      let newObj1 = obj1[el];
      let newObj2 = obj2[el];
      return newEqual(newObj1, newObj2);
    } else if (typeEl1 !== typeEl2) {
      return false;
    }
  }
  return true;
}
console.log(newEqual(obj1, obj2));
console.log(newEqual(obj3, obj2));
console.log(newEqual(obj1, obj4));

newEqual(obj1, obj2);





