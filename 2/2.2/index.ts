// // !Задание 2. Объединение типов и массивов

// type Arr1 = (Number | null)[]
// const arr1:Arr1 = [1, 2, 3, null]; 

// type Arr2 = (string | boolean)[]
// const arr2:Arr2 = ['safety', '=', true]

// type Arr3 = (Number[] | string[])[]
// const arr3:Arr3 = [
//   [1, 2, 3, 4, 5],
//   ['1', '2', '3', '4', '5'],
// ]

// type Arr4 = (Number | string | boolean | undefined)[]
// const arr4:Arr4 = [
//   1, 2, true, 'str', undefined
// ] 

// type Arr5 = {id:number,name:string}[]
// const arr5:Arr5 = [
//   {
//     id: 1,
//     name: 'Студент',
//   },
//   {
//     id: 2,
//     name: 'Наставник',
//   }
// ]